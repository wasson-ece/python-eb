# python-eb

A python image used to deploy to AWS ElasticBeanstalk in Bitbucket Pipelines

This image can be used rather than the python image listed in [AWS Labs' ElasticBeanstalk deployment repo](https://bitbucket.org/awslabs/aws-elastic-beanstalk-bitbucket-pipelines-python/src/master/) in order to improve Pipeline time.

It can be used as [wassonece/python-eb](https://hub.docker.com/r/wassonece/python-eb/)
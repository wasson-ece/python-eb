FROM python:3.5.1

RUN apt-get update && apt-get install -y \
    zip
RUN pip install boto3==1.3.0